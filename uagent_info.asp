<%
' *******************************************
' Copyright 2010-2011, Anthony Hand
'
' File version date: October 5, 2011
'		Update: 
'		- Initial version translated from PHP released on August 22, 2011
'
' LICENSE INFORMATION
' Licensed under the Apache License, Version 2.0 (the "License"); 
' you may not use this file except in compliance with the License. 
' You may obtain a copy of the License at 
'        http:'www.apache.org/licenses/LICENSE-2.0 
' Unless required by applicable law or agreed to in writing, 
' software distributed under the License is distributed on an 
' "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
' either express or implied. See the License for the specific 
' language governing permissions and limitations under the License. 
'
'
' ABOUT THIS PROJECT
'   Project Owner: Anthony Hand
'   Email: anthony.hand@gmail.com
'   Web Site: http://www.mobileesp.com
'   Source Files: http:'code.google.com/p/mobileesp/
'   
'   Versions of this code are available for:
'      PHP, JavaScript, Java, ASP (VBScript), ASP.NET (C#), and Ruby
'
' *******************************************

' *******************************************
' The uagent_info class encapsulates information about
'   a browser's connection to your web site. 
'   You can use it to find out whether the browser asking for
'   your site's content is probably running on a mobile device.
'   The methods were written so you can be as granular as you want.
'   For example, enquiring whether it's as specific as an iPod Touch or
'   as general as a smartphone class device.
'   The object's methods return true or false.

Class uagent_info

	Private useragent
	Private httpAccept
	
    Private isIphone
	Private isAndroidPhone
	Private isTierTablet
	Private isTierIphone
	Private isTierRichCss
	Private isTierGenericMobile

	Private engineWebKit
	Private deviceIphone
	Private deviceIpod
	Private deviceIpad
	Private deviceMacPpc
	
	Private deviceAndroid
	Private deviceGoogleTV
	Private deviceXoom
	Private deviceHtcFlyer

	Private deviceNuvifone

	Private deviceSymbian
	Private deviceS60
	Private deviceS70
	Private deviceS80
	Private deviceS90

	Private deviceWinPhone7
	Private deviceWinMob
	Private deviceWindows
	Private deviceIeMob
	Private devicePpc
	Private enginePie

	Private deviceBB
	Private vndRIM
	Private deviceBBStorm
	Private deviceBBBold
	Private deviceBBTour
	Private deviceBBCurve
	Private deviceBBTorch
	Private deviceBBPlaybook

	Private devicePalm
	Private deviceWebOS
	Private deviceWebOShp
	Private engineBlazer
	Private engineXiino

	Private deviceKindle

	Private vndwap
	Private wml

	Private deviceTablet
	Private deviceBrew
	Private deviceDanger
	Private deviceHiptop
	Private devicePlaystation
	Private deviceNintendoDs
	Private deviceNintendo
	Private deviceWii
	Private deviceXbox
	Private deviceArchos

	Private engineOpera
	Private engineNetfront
	Private engineUpBrowser
	Private engineOpenWeb
	Private deviceMidp
	Private uplink
	Private engineTelecaQ

	Private devicePda
	Private mini
	Private mobile
	Private mobi

	Private maemo
	Private linux
	Private qtembedded
	Private mylocom2

	Private manuSonyEricsson
	Private manuericsson
	Private manuSamsung1
	Private manuSony
	Private manuHtc

	Private svcDocomo
	Private svcKddi
	Private svcVodafone

	Private disUpdate

	'**************************
	'The constructor. Initializes several default variables.
	Private Sub Class_Initialize
	
		'Let"s store values for quickly accessing the same info multiple times.
		isIphone = 0 'Stores whether the device is an iPhone or iPod Touch.
		isAndroidPhone = 0 'Stores whether the device is a (small-ish) Android phone or media player.
		isTierTablet = 0 'Stores whether is the Tablet (HTML5-capable, larger screen) tier of devices.
		isTierIphone = 0 'Stores whether is the iPhone tier of devices.
		isTierRichCss = 0 'Stores whether the device can probably support Rich CSS, but JavaScript support is not assumed. (e.g., newer BlackBerry, Windows Mobile)
		isTierGenericMobile = 0 'Stores whether it is another mobile device, which cannot be assumed to support CSS or JS (eg, older BlackBerry, RAZR)

		'Initialize some initial smartphone string variables.
		engineWebKit = "webkit"
		deviceIphone = "iphone"
		deviceIpod = "ipod"
		deviceIpad = "ipad"
		deviceMacPpc = "macintosh" 'Used for disambiguation

		deviceAndroid = "android"
		deviceGoogleTV = "googletv"
		deviceXoom = "xoom" 'Motorola Xoom
		deviceHtcFlyer = "htc_flyer" 'HTC Flyer

		deviceNuvifone = "nuvifone" 'Garmin Nuvifone

		deviceSymbian = "symbian"
		deviceS60 = "series60"
		deviceS70 = "series70"
		deviceS80 = "series80"
		deviceS90 = "series90"

		deviceWinPhone7 = "windows phone os 7" 
		deviceWinMob = "windows ce"
		deviceWindows = "windows" 
		deviceIeMob = "iemobile"
		devicePpc = "ppc" 'Stands for PocketPC
		enginePie = "wm5 pie" 'An old Windows Mobile

		deviceBB = "blackberry"   
		vndRIM = "vnd.rim" 'Detectable when BB devices emulate IE or Firefox
		deviceBBStorm = "blackberry95"  'Storm 1 and 2
		deviceBBBold = "blackberry97" 'Bold
		deviceBBTour = "blackberry96" 'Tour
		deviceBBCurve = "blackberry89" 'Curve2
		deviceBBTorch = "blackberry 98" 'Torch
		deviceBBPlaybook = "playbook" 'PlayBook tablet

		devicePalm = "palm"
		deviceWebOS = "webos" 'For Palm"s line of WebOS devices
		deviceWebOShp = "hpwos" 'For HP"s line of WebOS devices

		engineBlazer = "blazer" 'Old Palm browser
		engineXiino = "xiino" 'Another old Palm

		deviceKindle = "kindle" 'Amazon Kindle, eInk one.

		'Initialize variables for mobile-specific content.
		vndwap = "vnd.wap"
		wml = "wml"   

		'Initialize variables for other random devices and mobile browsers.
		deviceTablet = "tablet" 'Generic term for slate and tablet devices
		deviceBrew = "brew"
		deviceDanger = "danger"
		deviceHiptop = "hiptop"
		devicePlaystation = "playstation"
		deviceNintendoDs = "nitro"
		deviceNintendo = "nintendo"
		deviceWii = "wii"
		deviceXbox = "xbox"
		deviceArchos = "archos"

		engineOpera = "opera" 'Popular browser
		engineNetfront = "netfront" 'Common embedded OS browser
		engineUpBrowser = "up.browser" 'common on some phones
		engineOpenWeb = "openweb" 'Transcoding by OpenWave server
		deviceMidp = "midp" 'a mobile Java technology
		uplink = "up.link"
		engineTelecaQ = "teleca q" 'a modern feature phone browser

		devicePda = "pda" 'some devices report themselves as PDAs
		mini = "mini"  'Some mobile browsers put "mini" in their names.
		mobile = "mobile" 'Some mobile browsers put "mobile" in their user agent strings.
		mobi = "mobi" 'Some mobile browsers put "mobi" in their user agent strings.

		'Use Maemo, Tablet, and Linux to test for Nokia"s Internet Tablets.
		maemo = "maemo"
		linux = "linux"
		qtembedded = "qt embedded" 'for Sony Mylo and others
		mylocom2 = "com2" 'for Sony Mylo also

		'In some UserAgents, the only clue is the manufacturer.
		manuSonyEricsson = "sonyericsson"
		manuericsson = "ericsson"
		manuSamsung1 = "sec-sgh"
		manuSony = "sony"
		manuHtc = "htc" 'Popular Android and WinMo manufacturer

		'In some UserAgents, the only clue is the operator.
		svcDocomo = "docomo"
		svcKddi = "kddi"
		svcVodafone = "vodafone"

		'Disambiguation strings.
		disUpdate = "update" 'pda vs. update
	
		useragent = Request.ServerVariables("HTTP_USER_AGENT")
		httpAccept = Request.ServerVariables("HTTP_ACCEPT")
		
		'Let's initialize some values to save cycles later.
		call InitDeviceScan
		
	End Sub
	
	'**************************
	' Initialize Key Stored Values.
	Public Function InitDeviceScan
        'We'll use these 4 variables to speed other processing. They're super common.
		isIphone = DetectIphoneOrIpod()
        isAndroidPhone = DetectAndroidPhone()
        isTierIphone = DetectTierIphone()
        isTierTablet = DetectTierTablet()
		
		'Optional: Comment these out if you don't need them.
        isTierRichCss = DetectTierRichCss()
        isTierGenericMobile = DetectTierOtherPhones()
	End Function	

	'**************************
	'Returns the contents of the User Agent value, in lower case.
	Public Function Get_Uagent
		Get_Uagent = useragent
	End Function

	'**************************
	'Returns the contents of the HTTP Accept value, in lower case.
	Public Function Get_HttpAccept
		Get_HttpAccept = httpAccept
	End Function
	
	'**************************
	' Detects if the current device is an iPhone.
	Public Function DetectIphone
		if InStr(1, useragent, deviceIphone, 1) > 0 then
			'The iPad and iPod Touch say they're an iPhone. So let's disambiguate.
			if (DetectIpad() = true or DetectIpod() = true) then
				DetectIphone = false
				Exit Function
			'Yay! It's an iPhone!
			else
				DetectIphone = true
				Exit Function
			end if
        else
			DetectIphone = false
            Exit Function
		end if
	End Function
	
	'**************************
	' Detects if the current device is an iPod Touch.
	Public Function DetectIpod
		if InStr(1, useragent, deviceIpod, 1) > 0 then
			DetectIpod = true
			Exit Function
		else
			DetectIpod = false
			Exit Function
		end if
	End Function
	
	'**************************
	' Detects if the current device is an iPad tablet.
	Public Function DetectIpad
		if (InStr(1, useragent, deviceIpad, 1) > 0 and _
		   DetectWebkit() = true) then
			DetectIpad = true
			Exit Function
		else
			DetectIpad = false
			Exit Function
		end if
	End Function
	
	'**************************
	' Detects if the current device is an iPhone or iPod Touch.
	Public Function DetectIphoneOrIpod
		'We repeat the searches here because some iPods may report themselves as an iPhone, which would be okay.
		if (InStr(1, useragent, deviceIphone, 1) > 0 or _
		   InStr(1, useragent, deviceIpod, 1) > 0) then
			DetectIphoneOrIpod = true
			Exit Function
		else
			DetectIphoneOrIpod = false
			Exit Function
		end if
	End Function

	'**************************
    ' Detects *any* iOS device: iPhone, iPod Touch, iPad.
	Public Function DetectIos
		if (DetectIphoneOrIpod() = true or _
		   DetectIpad() = true) then
			DetectIos = true
			Exit Function
		else
			DetectIos = false
			Exit Function
		end if
	End Function
	
	'**************************
	' Detects *any* Android OS-based device: phone, tablet, and multi-media player.
	' Also detects Google TV.
	Public Function DetectAndroid
		if (InStr(1, useragent, deviceAndroid, 1) > 0 or _
		   DetectGoogleTV() = true) then
			DetectAndroid = true
			Exit Function
		end if
		'Special check for the HTC Flyer 7" tablet
		if InStr(1, useragent, deviceHtcFlyer, 1) > 0 then
			DetectAndroid = true
			Exit Function
		else
			DetectAndroid = false
			Exit Function
		end if
	End Function
	
	'**************************
	' Detects if the current device is a (small-ish) Android OS-based device
	' used for calling and/or multi-media (like a Samsung Galaxy Player).
	' Google says these devices will have 'Android' AND 'mobile' in user agent.
	' Ignores tablets (Honeycomb and later).
	Public Function DetectAndroidPhone
		if (DetectAndroid() = true and _
		   InStr(1, useragent, deviceAndroid, 1) > 0 ) then
			DetectAndroidPhone = true
			Exit Function
		end if
		'Special check for Android phones with Opera Mobile. They should report here.
		if DetectOperaAndroidPhone() = true then
			DetectAndroidPhone = true
			Exit Function
		end if
		'Special check for the HTC Flyer 7" tablet. It should report here.
		if InStr(1, useragent, deviceHtcFlyer, 1) > 0 then
			DetectAndroidPhone = true
			Exit Function
		else
			DetectAndroidPhone = false
			Exit Function
		end if
	End Function
   
	'**************************
	' Detects if the current device is a (self-reported) Android tablet.
	' Google says these devices will have 'Android' and NOT 'mobile' in their user agent.
	Public Function DetectAndroidTablet
		'First, let's make sure we're on an Android device.
		if DetectAndroid() = false then
			DetectAndroidTablet = false
			Exit Function
		end if
		'Special check for Opera Android Phones. They should NOT report here.
		if DetectOperaMobile() = true then
			DetectAndroidTablet = false
			Exit Function
		end if
		'Special check for the HTC Flyer 7" tablet. It should NOT report here.
		if InStr(1, useragent, deviceHtcFlyer, 1) > 0 then
			DetectAndroidTablet = false
			Exit Function
		end if
		'Otherwise, if it's Android and does NOT have 'mobile' in it, Google says it's a tablet.
		if InStr(1, useragent, mobile, 1) > 0 then
			DetectAndroidTablet = false
			Exit Function
		else
			DetectAndroidTablet = true
			Exit Function
		end if
	End Function
	
	'**************************
	' Detects if the current device is an Android OS-based device and
	' the browser is based on WebKit.
	Public Function DetectAndroidWebKit
		if (DetectAndroid() = true and _
		   DetectWebkit() = true) then
			DetectAndroidWebKit = true
			Exit Function
		else
			DetectAndroidWebKit = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current device is a GoogleTV.
	Public Function DetectGoogleTV
		if InStr(1, useragent, deviceGoogleTV, 1) > 0 then
			DetectGoogleTV = true
			Exit Function
		else
			DetectGoogleTV = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is based on WebKit.
	Public Function DetectWebkit
		if InStr(1, useragent, engineWebKit, 1) > 0 then
			DetectWebkit = true
			Exit Function
		else
			DetectWebkit = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is the Nokia S60 Open Source Browser.
	Public Function DetectS60OssBrowser
		'First, test for WebKit, then make sure it's either Symbian or S60.
		if DetectWebkit() = true then
			if (InStr(1, useragent, deviceSymbian, 1) > 0 or _
			   InStr(1, useragent, deviceS60, 1) > 0) then 
				DetectS60OssBrowser = true
				Exit Function
			else
				DetectS60OssBrowser = false
				Exit Function
			end if
		else
			DetectS60OssBrowser = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current device is any Symbian OS-based device,
	' including older S60, Series 70, Series 80, Series 90, and UIQ, 
	' or other browsers running on these devices.
	Public Function DetectSymbianOS
		if (InStr(1, useragent, deviceSymbian, 1) > 0 or _
		   InStr(1, useragent, deviceS60, 1) > 0 or _
		   InStr(1, useragent, deviceS70, 1) > 0 or _
		   InStr(1, useragent, deviceS80, 1) > 0 or _
		   InStr(1, useragent, deviceS90, 1) > 0) then
			DetectSymbianOS = true
			Exit Function
		else
			DetectSymbianOS = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is a 
	' Windows Phone 7 device.
	Public Function DetectWindowsPhone7
		if InStr(1, useragent, deviceWinPhone7, 1) > 0 then
			DetectWindowsPhone7 = true
			Exit Function
		else
			DetectWindowsPhone7 = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is a Windows Mobile device.
	' Excludes Windows Phone 7 devices. 
	' Focuses on Windows Mobile 6.xx and earlier.
	Public Function DetectWindowsMobile
		if DetectWindowsPhone7() = true then
			DetectWindowsMobile = false
			Exit Function
		end if
		'Most devices use 'Windows CE', but some report 'iemobile' 
		'  and some older ones report as 'PIE' for Pocket IE. 
		if InStr(1, useragent, deviceWinMob, 1) > 0 or _
		   InStr(1, useragent, deviceIeMob, 1) > 0 or _
		   InStr(1, useragent, enginePie, 1) > 0 then
			DetectWindowsMobile = true
			Exit Function
		end if
		'Test for Windows Mobile PPC but not old Macintosh PowerPC.
		if InStr(1, useragent, devicePpc, 1) > 0 and _ 
		   Not InStr(1, useragent, deviceMacPpc, 1) > 0 then		
			DetectWindowsMobile = true
			Exit Function
		end if
		'Test for certain Windwos Mobile-based HTC devices.
		if (InStr(1, useragent, manuHtc, 1) > 0 and _
		   InStr(1, useragent, deviceWindows, 1) > 0) then
			DetectWindowsMobile = true
			Exit Function
		end if
		if (DetectWapWml() = true and _
		   InStr(1, useragent, deviceWindows, 1) > 0) then
			DetectWindowsMobile = true
			Exit Function
		else
			DetectWindowsMobile = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is any BlackBerry device.
	' Includes the PlayBook.
	Public Function DetectBlackBerry
		if (InStr(1, useragent, deviceBB, 1) > 0 or _
		   InStr(1, httpaccept, vndRIM, 1) > 0) then
			DetectBlackBerry = true
			Exit Function
		else
			DetectBlackBerry = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is on a BlackBerry tablet device.
	'    Examples: PlayBook
	Public Function DetectBlackBerryTablet
		if InStr(1, useragent, deviceBBPlaybook, 1) > 0 then
			DetectBlackBerryTablet = true
			Exit Function
		else
			DetectBlackBerryTablet = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is a BlackBerry phone device AND uses a
	'    WebKit-based browser. These are signatures for the new BlackBerry OS 6.
	'    Examples: Torch. Includes the Playbook.
	Public Function DetectBlackBerryWebKit
		if (DetectBlackBerry() = true and _
		   DetectWebkit() = true) then
			DetectBlackBerryWebKit = true
			Exit Function
		else
			DetectBlackBerryWebKit = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is a BlackBerry Touch phone
	'    device, such as the Storm or Torch. Excludes the Playbook.
	Public Function DetectBlackBerryTouch
		if (InStr(1, useragent, deviceBBStorm, 1) > 0 or _
		   InStr(1, useragent, deviceBBTorch, 1) > 0) then
			DetectBlackBerryTouch = true
			Exit Function
		else
			DetectBlackBerryTouch = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is a BlackBerry OS 5 device AND
	'    has a more capable recent browser. Excludes the Playbook.
	'    Examples, Storm, Bold, Tour, Curve2
	'    Excludes the new BlackBerry OS 6 browser!!
	Public Function DetectBlackBerryHigh
		'Disambiguate for BlackBerry OS 6 (WebKit) browser
		if DetectBlackBerryWebKit() = true then
			DetectBlackBerryHigh = false
			Exit Function
		end if
		if DetectBlackBerry() = true then
			if (DetectBlackBerryTouch() = true or _
			   InStr(1, useragent, deviceBBBold, 1) > 0 or _
			   InStr(1, useragent, deviceBBTour, 1) > 0 or _
			   InStr(1, useragent, deviceBBCurve, 1) > 0) then
				DetectBlackBerryHigh = true
				Exit Function
			else
				DetectBlackBerryHigh = false
				Exit Function
			end if
		else
			DetectBlackBerryHigh = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is a BlackBerry device AND
	'    has an older, less capable browser. 
	'    Examples: Pearl, 8800, Curve1.
	Public Function DetectBlackBerryLow
		if DetectBlackBerry() = true then
			'Assume that if it's not in the High tier, then it's Low.
			if (DetectBlackBerryHigh() = true or _
			   DetectBlackBerryWebKit() = true) then
				DetectBlackBerryLow = false
				Exit Function
			else
				DetectBlackBerryLow = true
				Exit Function
			end if
		else
			DetectBlackBerryLow = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is on a PalmOS device.
	Public Function DetectPalmOS
		'Most devices nowadays report as 'Palm', but some older ones reported as Blazer or Xiino.
		if (InStr(1, useragent, devicePalm, 1) > 0 or _
		   InStr(1, useragent, engineBlazer, 1) > 0 or _
		   InStr(1, useragent, engineXiino, 1) > 0) then
		    'Make sure it's not WebOS first
			if DetectPalmWebOS() = true then
				DetectPalmOS = false
				Exit Function
			else
				DetectPalmOS = true
				Exit Function
			end if
		else
			DetectPalmOS = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is on a Palm device
	'   running the new WebOS.
	Public Function DetectPalmWebOS
		if InStr(1, useragent, deviceWebOS, 1) > 0 then
			DetectPalmWebOS = true
			Exit Function
		else
			DetectPalmWebOS = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is on an HP tablet running WebOS.
	Public Function DetectWebOSTablet
		if (InStr(1, useragent, deviceWebOShp, 1) > 0 and _
		   InStr(1, useragent, deviceTablet, 1) > 0) then
			DetectWebOSTablet = true
			Exit Function
		else
			DetectWebOSTablet = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is a
	'   Garmin Nuvifone.
	Public Function DetectGarminNuvifone
		if InStr(1, useragent, deviceNuvifone, 1) > 0 then
			DetectGarminNuvifone = true
			Exit Function
		else
			DetectGarminNuvifone = false
			Exit Function
		end if
	End Function

	'**************************
	' Check to see whether the device is any device
	'   in the 'smartphone' category.
	Public Function DetectSmartphone
		if (isIphone = true or _
		   isAndroidPhone = true or _
		   isTierIphone = true or _
		   DetectS60OssBrowser() = true or _
		   DetectSymbianOS() = true or _
		   DetectWindowsMobile() = true or _
		   DetectWindowsPhone7() = true or _
		   DetectBlackBerry() = true or _
		   DetectPalmWebOS() = true or _
		   DetectPalmOS() = true or _
		   DetectGarminNuvifone() = true) then
			DetectSmartphone = true
			Exit Function
		else
			DetectSmartphone = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects whether the device is a Brew-powered device.
	Public Function DetectBrewDevice
		if InStr(1, useragent, deviceBrew, 1) > 0 then
			DetectBrewDevice = true
			Exit Function
		else
			DetectBrewDevice = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects the Danger Hiptop device.	
	Public Function DetectDangerHiptop
		if (InStr(1, useragent, deviceDanger, 1) > 0 or _
		   InStr(1, useragent, deviceHiptop, 1) > 0) then
			DetectDangerHiptop = true
			Exit Function
		else
			DetectDangerHiptop = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is Opera Mobile or Mini.
	Public Function DetectOperaMobile
		if InStr(1, useragent, engineOpera, 1) > 0 then
			if (InStr(1, useragent, mini, 1) > 0 or _
			   InStr(1, useragent, mobi, 1) > 0 ) then
				DetectOperaMobile = true
				Exit Function
			else
				DetectOperaMobile = false
				Exit Function
			end if
		else
			DetectOperaMobile = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is Opera Mobile
	' running on an Android phone.
	Public Function DetectOperaAndroidPhone
		if (InStr(1, useragent, engineOpera, 1) > 0 and _
		   InStr(1, useragent, deviceAndroid, 1) > 0 and _
		   InStr(1, useragent, mobi, 1) > 0) then
			DetectOperaAndroidPhone = true
			Exit Function
		else
			DetectOperaAndroidPhone = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is Opera Mobile
	' running on an Android tablet.  
	Public Function DetectOperaAndroidTablet
		if (InStr(1, useragent, engineOpera, 1) > 0 and _
		   InStr(1, useragent, deviceAndroid, 1) > 0 and _
		   InStr(1, useragent, deviceTablet, 1) > 0) then
			DetectOperaAndroidTablet = true
			Exit Function
		else
			DetectOperaAndroidTablet = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects whether the device supports WAP or WML.
	Public Function DetectWapWml
		if (InStr(1, httpaccept, vndwap, 1) > 0 or _
		   InStr(1, httpaccept, wml, 1) > 0) then
			DetectWapWml = true
			Exit Function
		else
			DetectWapWml = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current device is an Amazon Kindle.
	Public Function DetectKindle
		if InStr(1, useragent, deviceKindle, 1) > 0 then
			DetectKindle = true
			Exit Function
		else
			DetectKindle = false
			Exit Function
		end if
	End Function

	'**************************
	' The quick way to detect for a mobile device.
	' Will probably detect most recent/current mid-tier Feature Phones
	' as well as smartphone-class devices. Excludes Apple iPads and other modern tablets.
	Public Function DetectMobileQuick
		'Let's exclude tablets
		if isTierTablet = true then
			DetectMobileQuick = false
			Exit Function
		end if
		'Most mobile browsing is done on smartphones
		if DetectSmartphone() = true then
			DetectMobileQuick = true
			Exit Function
		end if
		if (DetectWapWml() = true or _
		   DetectBrewDevice() = true or _
		   DetectOperaMobile() = true) then
			DetectMobileQuick = true
			Exit Function
		end if
		if (InStr(1, useragent, engineNetfront, 1) > 0 or _
		   InStr(1, useragent, engineUpBrowser, 1) > 0 or _
		   InStr(1, useragent, engineOpenWeb, 1) > 0) then
			DetectMobileQuick = true
			Exit Function
		end if
		if (DetectDangerHiptop() = true or _
		   DetectMidpCapable() = true or _
		   DetectMaemoTablet() = true or _
		   DetectArchos() = true) then
			DetectMobileQuick = true
			Exit Function
		end if
		if (InStr(1, useragent, devicePda, 1) > 0 and _
		   not InStr(1, useragent, disUpdate, 1) > 0) then
			DetectMobileQuick = true
			Exit Function
		end if
		if InStr(1, useragent, mobile, 1) > 0 then
			DetectMobileQuick = true
			Exit Function
		else
			DetectMobileQuick = false
			Exit Function
		end if
	End Function

	'**************************
    ' Detects if the current device is a Sony Playstation.
	Public Function DetectSonyPlaystation
		if InStr(1, useragent, devicePlaystation, 1) > 0 then
			DetectSonyPlaystation = true
			Exit Function
		else
			DetectSonyPlaystation = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current device is a Nintendo game device.
	Public Function DetectNintendo
		if (InStr(1, useragent, deviceNintendo, 1) > 0 or _
		   InStr(1, useragent, deviceWii, 1) > 0 or _
		   InStr(1, useragent, deviceNintendoDs, 1) > 0) then
			DetectNintendo = true
			Exit Function
		else
			DetectNintendo = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current device is a Microsoft Xbox.
	Public Function DetectXbox
		if InStr(1, useragent, deviceXbox, 1) > 0 then
			DetectXbox = true
			Exit Function
		else
			DetectXbox = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current device is an Internet-capable game console.
	Public Function DetectGameConsole
		if DetectSonyPlaystation() = true then
			DetectGameConsole = true
			Exit Function
		elseif DetectNintendo() = true then
			DetectGameConsole = true
			Exit Function
		elseif DetectXbox() = true then
			DetectGameConsole = true
			Exit Function
		else
			DetectGameConsole = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current device supports MIDP, a mobile Java technology.
	Public Function DetectMidpCapable
		if (InStr(1, useragent, deviceMidp, 1) > 0 or _
		   InStr(1, httpaccept, deviceMidp, 1) > 0) then
			DetectMidpCapable = true
			Exit Function
		else
			DetectMidpCapable = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current device is on one of the Maemo-based Nokia Internet Tablets.
	Public Function DetectMaemoTablet
		if InStr(1, useragent, maemo, 1) > 0 then
			DetectMaemoTablet = true
			Exit Function
		end if
		
		if (InStr(1, useragent, linux, 1) > 0 and _
		   InStr(1, useragent, deviceTablet, 1) > 0 and _
		   DetectWebOSTablet() = false and _
		   DetectAndroid() = false) then
			DetectMaemoTablet = true
			Exit Function
		else
			DetectMaemoTablet = false
			Exit Function
		end if
	End Function
   
	'**************************
	' Detects if the current device is an Archos media player/Internet tablet.
	Public Function DetectArchos
		if InStr(1, useragent, deviceArchos, 1) > 0 then
			DetectArchos = true
			Exit Function
		else
			DetectArchos = false
			Exit Function
		end if
	End Function

	'**************************
	' Detects if the current browser is a Sony Mylo device.
	Public Function DetectSonyMylo
		if InStr(1, useragent, manuSony, 1) > 0 then
			if (InStr(1, useragent, qtembedded, 1) > 0 or _
			   InStr(1, useragent, mylocom2, 1) > 0) then
				DetectSonyMylo = true
				Exit Function
			else
				DetectSonyMylo = false
				Exit Function
			end if
		else
			DetectSonyMylo = false
			Exit Function
		end if
	End Function

	'**************************
	' The longer and more thorough way to detect for a mobile device.
	'   Will probably detect most feature phones,
	'   smartphone-class devices, Internet Tablets, 
	'   Internet-enabled game consoles, etc.
	'   This ought to catch a lot of the more obscure and older devices, also --
	'   but no promises on thoroughness!
	Public Function DetectMobileLong
		if DetectMobileQuick() = true then
			DetectMobileLong = true
			Exit Function
		end if
		if DetectGameConsole() = true then
			DetectMobileLong = true
			Exit Function
		end if
		if DetectSonyMylo() = true then
			DetectMobileLong = true
			Exit Function
		end if
		if InStr(1, useragent, uplink, 1) > 0 then
			DetectMobileLong = true
			Exit Function
		end if
		if InStr(1, useragent, manuSonyEricsson, 1) > 0 then
			DetectMobileLong = true
			Exit Function
		end if
		if InStr(1, useragent, manuericsson, 1) > 0 then
			DetectMobileLong = true
			Exit Function
		end if
		if InStr(1, useragent, manuSamsung1, 1) > 0 then
			DetectMobileLong = true
			Exit Function
		end if
		if InStr(1, useragent, svcDocomo, 1) > 0 then
			DetectMobileLong = true
			Exit Function
		end if
		if InStr(1, useragent, svcKddi, 1) > 0 then
			DetectMobileLong = true
			Exit Function
		end if
		if InStr(1, useragent, svcVodafone, 1) > 0 then
			DetectMobileLong = true
			Exit Function
		else
			DetectMobileLong = false
			Exit Function
		end if
	End Function
	
	'*****************************
	' For Mobile Web Site Design
	'*****************************

	'**************************
	' The quick way to detect for a tier of devices.
	'   This method detects for the new generation of
	'   HTML 5 capable, larger screen tablets.
	'   Includes iPad, Android (e.g., Xoom), BB Playbook, WebOS, etc.
	Public Function DetectTierTablet
		if (DetectIpad() = true or _
		   DetectAndroidTablet() = true or _
		   DetectBlackBerryTablet() = true or _
		   DetectWebOSTablet() = true) then
			DetectTierTablet = true
			Exit Function
		else
			DetectTierTablet = false
			Exit Function
		end if
	End Function
	
	'**************************
	' The quick way to detect for a tier of devices.
	'   This method detects for devices which can 
	'   display iPhone-optimized web content.
	'   Includes iPhone, iPod Touch, Android, WebOS, etc.
	Public Function DetectTierIphone
		if (isIphone = true or _
		   isAndroidPhone = true) then
			DetectTierIphone = true
			Exit Function
		end if
		if (DetectBlackBerryWebKit() = true and _
		   DetectBlackBerryTouch() = true) then
			DetectTierIphone = true
			Exit Function
		end if
		if DetectPalmWebOS() = true then
			DetectTierIphone = true
			Exit Function
		end if
		if DetectGarminNuvifone() = true then
			DetectTierIphone = true
			Exit Function
		else
			DetectTierIphone = false
			Exit Function
		end if
	End Function
	
	'**************************
	' The quick way to detect for a tier of devices.
	'   This method detects for devices which are likely to be capable 
	'   of viewing CSS content optimized for the iPhone, 
	'   but may not necessarily support JavaScript.
	'   Excludes all iPhone Tier devices.
	Public Function DetectTierRichCss
		if DetectMobileQuick() = true then
			if DetectTierIphone() = true then
				DetectTierRichCss = false
				Exit Function
			end if
			'The following devices are explicitly ok.
			if DetectWebkit() = true then
				DetectTierRichCss = true
				Exit Function
			end if
			if DetectS60OssBrowser() = true then
				DetectTierRichCss = true
				Exit Function
			end if
			'Note: 'High' BlackBerry devices ONLY
			if DetectBlackBerryHigh() = true then
				DetectTierRichCss = true
				Exit Function
			end if
			'WP7's IE-7-based browser isn't good enough for iPhone Tier. 
			if DetectWindowsPhone7() = true then
				DetectTierRichCss = true
				Exit Function
			end if
			if DetectWindowsMobile() = true then
				DetectTierRichCss = true
				Exit Function
			end if
			if InStr(1, useragent, engineTelecaQ, 1) > 0 then
				DetectTierRichCss = true
				Exit Function
			'default
			else
				DetectTierRichCss = false
				Exit Function
			end if
		else
			DetectTierRichCss = false
			Exit Function
		end if
	End Function
	
	'**************************
	' The quick way to detect for a tier of devices.
	'   This method detects for all other types of phones,
	'   but excludes the iPhone and RichCSS Tier devices.
	Public Function DetectTierOtherPhones
		'Exclude devices in the other 2 categories 
		if (DetectMobileLong() = true and _
		   DetectTierIphone() = false and _
		   DetectTierRichCss() = false) then
			DetectTierOtherPhones = true
			Exit Function
		else
			DetectTierOtherPhones = false
			Exit Function
		end if
	End Function
	
End Class
%>